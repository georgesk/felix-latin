HEADERS       = gafio.h
SOURCES       = gafio.cpp \
                main.cpp \
                felix.h \
                felix.cpp
install.files = INSTALL COPYING felix 
install.path = /usr/share/felix 
install.commands = $(SYMLINK) /usr/share/felix/felix /usr/bin/felix 

data.path = /usr/share/felix
data.files = Gaffiot.djvu help.tif

man.path=/usr/share/man/man1
man.files = felix.1.gz

INSTALLS += install
INSTALLS += data
INSTALLS += man
