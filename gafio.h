/*
*              gafio.h
*  © Yves Ouvrard (Collatinus), 2010
* =======================================================================
    This file is part of Felix 34.

    Felix 34 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Felix 34 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Felix 34.  If not, see <http://www.gnu.org/licenses/>.

* =======================================================================
*/

#ifndef GAFIO_H
#define GAFIO_H

#include <QObject>
#include <QMainWindow>
#include <QtGui/QScrollArea>

class QAction;
class QLineEdit;
class QFont;
class QLabel;
class QScrollArea;
class QScrollBar;
class QVBoxLayout;
class QHBoxLayout;
class QPushButton;
class Gxxxiv;

class zScrollArea : public QScrollArea{
  //zoomable scrollarea
  Q_OBJECT

public:
  zScrollArea(Gxxxiv * mainWindow);

protected:
  Gxxxiv * mainWindow;
  void wheelEvent (QWheelEvent *event);
};

class Gxxxiv : public QMainWindow
{
    Q_OBJECT

    friend class zScrollArea;

public:
    Gxxxiv();

private:
    QString message;
    QStringList historique;
    int folio;
    int pHist;

private slots:
    void about();
    void aide ();
    void bas ();
    void descend ();
    void droite ();
    void gauche ();
    void haut ();
    void hauteur ();
    void lCachee ();
    void lVisible ();
    void largeur ();
    void monte ();
    void page(int n);
    void pPrec ();
    void pSuiv ();
    void page_bas ();
    void page_haut ();
    void returnPressed (); 
protected slots:
    void zoomIn ();
    void zoomOut ();

private:
    void scaleImage();
    void adjustScrollBar(QScrollBar *scrollBar, double factor);

    QWidget *commandes;
    QLineEdit *lSaisie;
    QFont *fonte;
    QHBoxLayout *horizontalLayout;
    QPushButton *bCh;
    QPushButton *bPrec;
    QPushButton *bSuiv;
    QPushButton *fleche;
    QLabel *imageLabel;
    zScrollArea *scrollArea;
    QPixmap pix;
    QVBoxLayout *layout;
    QPoint lastPos;
    double scaleFactor;

protected:
    void keyPressEvent (QKeyEvent *event);
    void mousePressEvent (QMouseEvent * event);
    void mouseMoveEvent (QMouseEvent * event);
};

#endif
