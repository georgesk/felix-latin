/****************************************************************************
**             main.cpp
*  © Yves Ouvrard (Collatinus), 2010
* =======================================================================
    This file is part of Felix 34.

    Felix 34 is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    Felix 34 is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with Felix 34.  If not, see <http://www.gnu.org/licenses/>.

* =======================================================================
*/

#include <QApplication>
//#include <QTextCodec>
#include "gafio.h"

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    //QTextCodec::setCodec(QTextCodec::codecForName("UTF-8"));
    Gxxxiv gxxxiv;
    gxxxiv.show();
    return app.exec();
}
